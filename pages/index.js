import React from 'react'
import Hero from '../components/Hero'
import HeroLeft from '../components/HeroLeft'
import HeroRight from '../components/HeroRight'
import OfferHero from '../components/OfferHero'
import Head from 'next/head'

const index = () => {
  return (
    <>
    <Head>
    <title>Elite Networks</title>
    </Head>
    <Hero/>
    <OfferHero/>
    <HeroLeft/>
    <HeroRight/>
    </>
  )
}

export default index