const mail = require('@sendgrid/mail');

mail.setApiKey(process.env.EMAIL_KEY);

export default function handler(req, res){


  const message = `Name:${(req.body.name)}\r\n`;

  
   const data = {
    to:'tntzxtech@gmail.com',
    from:'hello@ielite.xyz',
    subject:'Hello From Local Host',
    text:message,
    html:message.replace(/\r\n/g, '<br/>')
  }

  // mail.send(data);

  res.status(200).json({ name: message })
  
}
