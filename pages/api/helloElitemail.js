const mail = require('@sendgrid/mail');

mail.setApiKey(process.env.EMAIL_KEY);

export default function handler(req, res){

  
  const value = (
      
    JSON.parse(req.body)
    
    )


    let p = (
    `
    Name:${value.name}\r\n
    Email:${value.email}\r\n
    Phone:${value.mob}\r\n
    Country:${value.country}\r\n
    Time:${value.time}\r\n
    Body:${value.body}\r\n
    `
    )

    const data = {
      to:'tntzxtech@gmail.com',
      cc:'ashutosh.official@outlook.com',
      from:'hello@ielite.xyz',
      subject:'Lead From iElite Contact Form',
      text:p,
      html:p.replace(/\r\n/g, '<br/>')
    }
  
    // console.log(data)
    mail.send(data)

    res.status(200).json({response:"a"})
  // }  

}


