import React from 'react'
import Faq from '../components/Faq'
import PriceCard from '../components/PriceCard'
import Head from 'next/head'

const services = () => {
  return (
    <>
    <Head>
      <title>
        Services | iElite
      </title>
    </Head>
                                    

<div className="py-4 bg-gradient-to-br from-red-500 to-orange-600 md:bg-gradient-to-r">
    <div className="container m-auto px-6 text-center md:px-12 lg:px-20">
        <h2 className="mb-8 text-4xl text-white font-bold md:text-4xl">We belive in quality and timeline.</h2>
    </div>
</div>
    <PriceCard/>
    <Faq/>
    </>
  )
}

export default services