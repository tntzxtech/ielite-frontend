import '../styles/globals.css'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Router from 'next/router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css';
import Head from 'next/head';
import 'react-toastify/dist/ReactToastify.css';

//Binding events. 
Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

NProgress.configure({ showSpinner: false })

function MyApp({ Component, pageProps }) {
  return(
  <> 
  <Head>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
  </Head>
  <Header/>
  <Component {...pageProps}/>
  <Footer/>
  </>
  )
}

export default MyApp
