import Head from 'next/head'
import React from 'react'
import About from '../components/About'
import Hero from '../components/AboutHero'
import Features from '../components/Features'
import Ashutosh from '../components/Ashutosh'

const about = () => {
  return (
    <>
    <Head>
      <title>About | iElite</title>
    </Head>
    <Hero/>
    <Ashutosh/>
    <About/>
    <br/>
    <Features/>
    </>
  )
}

export default about