import React from 'react'
import Link from 'next/link'

const PriceCard = () => {

    const incl= [
        {
          name:"5 Years of Hosting just for 100$AUD- Renews @150$AUD/Yearly"
        },
        {
          name:"Dynamic Pages"
        },
        {
          name:"Responsive Design"
        },
        {
          name:"Animated Design"
        },
        {
          name:"SSL*"
        },
        {
          name:"Rest API"
        },
        {
          name:"Fully Secure from Attacks*"
        },
        {
          name:"MongoDB"
        },
        {
          name:"Mobile Friendly"
        },
        {
          name:"NextJs"
        },
        {
          name:"Blog System"
        },
        {
          name:"Google Addons"
        },
        {
          name:"Stock Images*"
        },
        {
          name:"Payment Gateway*"
        },
        {
          name:"Site Optimization"
        },
        {
          name:"Control Panel"
        },
      ]

    const notIncl = [
        {
          name:"Stress"
        },
        {
          name:"Management Headache"
        },
        {
          name:"Advanced - SEO"
        },
        {
          name:"Content Writing"
        },
      ]

    const incl2= [
        {
          name:"Responsive Pages"
        },
        {
          name:"Google Addons"
        },
        {
          name:"Product Integration"
        },
        {
          name:"Mobile Friendly"
        },
        {
          name:"Payment Gateway"
        },
      ]

    const notIncl2 = [
        {
          name:"Workload"
        },
        {
          name:"Site Optimization"
        },
        {
          name:"Content Writing"
        },
        {
          name:"Plugins & Themes*"
        }
      ]

  return (
    <>
    
<div className="relative mt-10 max-w-screen-xl mx-auto rounded-xl px-4 sm:px-6 shadow-xl border-2 border-indigo-400 lg:px-8">
    <div className="pricing-box max-w-lg mx-auto rounded-lg  overflow-hidden lg:max-w-none lg:flex">
        <div className="bg-white dark:bg-gray-800 px-6 py-8 lg:flex-shrink-1 lg:p-12">
            <h3 className="text-4xl leading-8 title-font font-bold text-gray-900 sm:text-3xl sm:leading-9 dark:text-white">
               <span className='text-indigo-600'>Tailor Made</span>- Fully developed for your business !
            </h3>
            <p className="mt-6 text-xl leading-6 text-gray-500 dark:text-gray-200">
                We study your business and architect the website that suits your buesiness to deliver best service and experince to your customer.
            </p>
            <div className="mt-8">
                <div className="flex items-center">
                    <h4 className="flex-shrink-0 pr-4 bg-white dark:bg-gray-800 text-sm leading-5 tracking-wider font-semibold uppercase text-indigo-600">
                        What&#x27;s included
                    </h4>
                    <div className="flex-1 border-t-2 border-gray-200">
                    </div>
                </div>
                <ul className="mt-8 lg:grid lg:grid-cols-2 lg:col-gap-8 lg:row-gap-5">

                {/* Features */}
                 
                {incl.map((item) => (
                    <li className="flex items-start p-1 lg:col-span-1" key={item.name}>
                        <div className="flex-shrink-0">
                            <svg className="h-6 w-6 mr-2" xmlns="http://www.w3.org/2000/svg" width="6" height="6" stroke="currentColor" fill="#10b981" viewBox="0 0 1792 1792">
                                <path d="M1412 734q0-28-18-46l-91-90q-19-19-45-19t-45 19l-408 407-226-226q-19-19-45-19t-45 19l-91 90q-18 18-18 46 0 27 18 45l362 362q19 19 45 19 27 0 46-19l543-543q18-18 18-45zm252 162q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z">
                                </path>
                            </svg>
                        </div>
                        <p className="ml-3 text-sm leading-5 text-gray-700 dark:text-gray-200">
                            {item.name}
                        </p>
                    </li>
                  ))}
                </ul>

            </div>


            <div className="mt-8">
                <div className="flex items-center">
                    <h4 className="flex-shrink-0 pr-4 bg-white text-sm dark:bg-gray-800 leading-5 tracking-wider font-semibold uppercase text-indigo-600">
                        &amp; What&#x27;s not
                    </h4>
                </div>
                <ul className="mt-8 lg:grid lg:grid-cols-2 lg:col-gap-8 lg:row-gap-5">
                {notIncl.map((item) => (
                    <li className="flex items-start lg:col-span-1" key={item.name}>
                        <div className="flex-shrink-0">
                            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="6" className="h-6 w-6 mr-2" fill="red" viewBox="0 0 1792 1792">
                                <path d="M1277 1122q0-26-19-45l-181-181 181-181q19-19 19-45 0-27-19-46l-90-90q-19-19-46-19-26 0-45 19l-181 181-181-181q-19-19-45-19-27 0-46 19l-90 90q-19 19-19 46 0 26 19 45l181 181-181 181q-19 19-19 45 0 27 19 46l90 90q19 19 46 19 26 0 45-19l181-181 181 181q19 19 45 19 27 0 46-19l90-90q19-19 19-46zm387-226q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z">
                                </path>
                            </svg>
                        </div>
                        <p className="ml-3 text-sm leading-5 text-gray-700 dark:text-gray-200">
                           {item.name}
                        </p>
                    </li>
                     ))}
                </ul>
            </div>
        </div>
        <div className="py-8 px-6 text-center bg-gray-50 dark:bg-gray-700 lg:flex-shrink-0 lg:flex lg:flex-col lg:justify-center lg:p-12">
            <img className='h-full' src='/we.png'></img>
            
            <p className="text-lg leading-6 font-bold text-gray-900 dark:text-white">
                Always ready to bargain!
            </p>
            <div className="mt-4 flex items-center justify-center text-5xl leading-none font-extrabold text-gray-900 dark:text-white">
                <Link href="/contact">
                <a>
                <button type="button" className="animate-bounce py-2 px-4  bg-orange-600 hover:bg-orange-700 focus:ring-indigo-500 focus:ring-offset-indigo-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                        Get quotations now
                </button>
                             </a>
                </Link>
       
            </div>
            <p className="mt-4 text-sm leading-5">
                <span className="block font-medium text-gray-500 dark:text-gray-400">
                    Patial Payments | Full Payments | Refunds
                </span>
                <span className=" inline-block font-medium text-gray-500 dark:text-gray-400">
                    Tax Excluded
                </span>
            </p>
            <div className="mt-6">
                <div className="rounded-md shadow">
                    {/* <button type="button" className="py-2 px-4  bg-indigo-600 hover:bg-indigo-700 focus:ring-indigo-500 focus:ring-offset-indigo-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                        Lets connect on call
                    </button> */}
                </div>
            </div>
        </div>
    </div>
</div>

{/* 
Second Card */}

<div className="relative mt-10 max-w-screen-xl mx-auto rounded-xl px-4 sm:px-6 shadow-xl border-2 border-indigo-400 lg:px-8">
    <div className="pricing-box max-w-lg mx-auto rounded-lg  overflow-hidden lg:max-w-none lg:flex">
        <div className="bg-white dark:bg-gray-800 px-6 py-8 lg:flex-shrink-1 lg:p-12">
            <h3 className="text-4xl leading-8 title-font font-bold text-gray-900 sm:text-3xl sm:leading-9 dark:text-white">
               <span className='text-indigo-600'>Designed</span>- Fully designed for your requirement!
            </h3>
            <p className="mt-6 text-base leading-6 text-gray-500 dark:text-gray-200">
            We understand your creative Ideas, We desing you website on any website designing platform to make your workflow go easier.
            </p>
            <div className="mt-8">
                <div className="flex items-center">
                    <h4 className="flex-shrink-0 pr-4 bg-white dark:bg-gray-800 text-sm leading-5 tracking-wider font-semibold uppercase text-indigo-600">
                        What&#x27;s included
                    </h4>
                    <div className="flex-1 border-t-2 border-gray-200">
                    </div>
                </div>
                <ul className="mt-8 lg:grid lg:grid-cols-2 lg:col-gap-8 lg:row-gap-5">

                {/* Features */}
                 
                {incl2.map((item) => (
                    <li className="flex items-start p-1 lg:col-span-1" key={item.name}>
                        <div className="flex-shrink-0">
                            <svg className="h-6 w-6 mr-2" xmlns="http://www.w3.org/2000/svg" width="6" height="6" stroke="currentColor" fill="#10b981" viewBox="0 0 1792 1792">
                                <path d="M1412 734q0-28-18-46l-91-90q-19-19-45-19t-45 19l-408 407-226-226q-19-19-45-19t-45 19l-91 90q-18 18-18 46 0 27 18 45l362 362q19 19 45 19 27 0 46-19l543-543q18-18 18-45zm252 162q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z">
                                </path>
                            </svg>
                        </div>
                        <p className="ml-3 text-sm leading-5 text-gray-700 dark:text-gray-200">
                            {item.name}
                        </p>
                    </li>
                  ))}
                </ul>

            </div>


            <div className="mt-8">
                <div className="flex items-center">
                    <h4 className="flex-shrink-0 pr-4 bg-white text-sm dark:bg-gray-800 leading-5 tracking-wider font-semibold uppercase text-indigo-600">
                        &amp; What&#x27;s not
                    </h4>
                </div>
                <ul className="mt-8 lg:grid lg:grid-cols-2 lg:col-gap-8 lg:row-gap-5">
                {notIncl2.map((item) => (
                    <li className="flex items-start lg:col-span-1" key={item.name}>
                        <div className="flex-shrink-0">
                            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="6" className="h-6 w-6 mr-2" fill="red" viewBox="0 0 1792 1792">
                                <path d="M1277 1122q0-26-19-45l-181-181 181-181q19-19 19-45 0-27-19-46l-90-90q-19-19-46-19-26 0-45 19l-181 181-181-181q-19-19-45-19-27 0-46 19l-90 90q-19 19-19 46 0 26 19 45l181 181-181 181q-19 19-19 45 0 27 19 46l90 90q19 19 46 19 26 0 45-19l181-181 181 181q19 19 45 19 27 0 46-19l90-90q19-19 19-46zm387-226q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z">
                                </path>
                            </svg>
                        </div>
                        <p className="ml-3 text-sm leading-5 text-gray-700 dark:text-gray-200">
                           {item.name}
                        </p>
                    </li>
                     ))}
                </ul>
            </div>
        </div>
        <div className="py-8 px-6 text-center bg-gray-50 dark:bg-gray-700 lg:flex-shrink-0 lg:flex lg:flex-col lg:justify-center lg:p-12">
            <img className='h-full' src='/we-2.png'></img>
            
            <p className="text-lg leading-6 font-bold text-gray-900 dark:text-white">
                Always ready to bargain!
            </p>
            <div className="mt-4 flex items-center justify-center text-5xl leading-none font-extrabold text-gray-900 dark:text-white">
                <Link href="/contact">
                  <a>
                <button type="button" className="animate-bounce py-2 px-4  bg-orange-600 hover:bg-orange-700 focus:ring-indigo-500 focus:ring-offset-indigo-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                        Get quotation now
                </button>
                </a>
                </Link>
       
            </div>
            <p className="mt-4 text-sm leading-5">
                <span className="block font-medium text-gray-500 dark:text-gray-400">
                    Patial Payments | Full Payments | Refunds
                </span>
                <span className=" inline-block font-medium text-gray-500 dark:text-gray-400">
                    Tax Excluded
                </span>
            </p>
            <div className="mt-6">
                <div className="rounded-md shadow">
                    {/* <button type="button" className="py-2 px-4  bg-indigo-600 hover:bg-indigo-700 focus:ring-indigo-500 focus:ring-offset-indigo-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                        Lets connect on call
                    </button> */}
                </div>
            </div>
        </div>
    </div>
</div>

    </>
  )
}

export default PriceCard