/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from 'react'
import { Popover, Transition } from '@headlessui/react'
import {
  MenuIcon,
  HomeIcon,
  UserGroupIcon, 
  PhoneIcon,
  PlayIcon,
  ViewGridIcon,
  XIcon,
} from '@heroicons/react/outline'
import Link from 'next/link'

//Define the menu name and links here

const link1 = "Home";
const link2 = "Services";
const link3 = "About";
const link4 = "Contact Us";



const solutions = [
  {
    name: link1,
    href: '/',
    icon: UserGroupIcon,
  },
  {
    name: link2,
    href: '/services',
    icon: UserGroupIcon,
  },
  {
    name: link3,
    href: '/about',
    icon: ViewGridIcon,
  },
  {
    name: link4,
    href: '/contact',
    icon: PhoneIcon,
  },
]
const callsToAction = [
  { name: 'Watch Demo', href: '#', icon: PlayIcon },
  { name: 'Contact Sales', href: '#', icon: PhoneIcon },
]
function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function header() {
  return (
    <Popover id="head" className="sticky top-0 backdrop-blur-sm  z-50">
    <div className="container sticky">
      <div className=" mx-auto px-4 sm:px-6">
        <div className="pl-5 pr-5 flex justify-between items-center border-b-2 border-gray-100 py-6 md:justify-start md:space-x-10">
          <Link href="/">
          <div className="flex justify-start lg:w-0 lg:flex-1">
            <a href="#">
              <span className="sr-only">Workflow</span>
              <img
                className="h-8 w-auto sm:h-10"
                src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
                alt=""
              />
            </a>
          </div>
          </Link>
          <div className="-mr-2 -my-2 md:hidden">
            <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
              <span className="sr-only">Open menu</span>
              <MenuIcon className="h-6 w-6" aria-hidden="true" />
            </Popover.Button>
          </div>

          <Popover.Group as="nav" className="hidden md:flex space-x-10">
         
            {/* //Main Menu On Desktop view */}

            {solutions.map((item) => (

            <Link href={item.href} key={item.href} >
            <a className="text-base font-medium text-gray-800 hover:text-gray-900">
            {item.name}
            </a>
            </Link>

           ))}

          </Popover.Group>

          <div className="hidden md:flex items-center justify-end md:flex-1 lg:w-0">
           <a
              href="tel:+91"
              className="ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-red-600	 hover:bg-red-700"
            >
            <PhoneIcon className="animate-pulse w-6 left-0 mr-2 text-white"/>
            Book a Call Now
            </a>
          </div>
        </div>
      </div>

      <Transition
        as={Fragment}
        enter="duration-200 ease-out"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="duration-100 ease-in"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <Popover.Panel focus className="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden">
          <div id="test" className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50">
            <div className="pt-5 pb-6 px-5">
              <div className="flex items-center justify-between">
                <div>
                  <img
                    className="h-8 w-auto"
                    src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
                    alt="Workflow"
                  />
                </div>
                <div className="-mr-2">
                  <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                    <span className="sr-only">Close menu</span>
                    <XIcon className="h-6 w-6" aria-hidden="true" />
                  </Popover.Button>
                </div>
              </div>

              {/* //Link for mobile view */}
              
              <div className="mt-6">
                <nav className="grid gap-y-8">
                  {solutions.map((item) => (
                    
                    <Link href={item.href} key={item.href}>
                    <a
                      key={item.name}
                      
                      className="-m-3 p-3 w-full flex items-center rounded-md hover:bg-gray-50"
                    >
                    <Popover.Button className="bg-white rounded-md p-2 w-full  inline-flex items-center justify-start text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                    
                      <item.icon className="flex-shrink-0 h-6 w-6 text-indigo-600" aria-hidden="true" />
                      <span className="ml-3 text-base font-medium text-gray-900" >{item.name}</span>        
                  
               
                  </Popover.Button>
                    </a>

                    </Link>
                  ))}
                      <a
              href="tel:+91"
              className="whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-red-600	 hover:bg-red-700"
            >
            <PhoneIcon className="w-6 left-0 mr-2 text-white"/>
            Book a Call Now
            </a>


                </nav>            
              </div>
            </div>
          </div>
        </Popover.Panel>
      </Transition>
    </div>
    </Popover>
  )
}
