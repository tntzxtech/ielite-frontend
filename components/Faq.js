import React from 'react'

const Faq = () => {
  return (
    <>
    <div className="bg-lightblue py-20 px-4">
        <div className="mx-auto max-w-6xl flex flex-col md:flex-row">
            <h2 className="animate-pulse mr-8 w-full md:w-1/3 text-3xl font-extrabold leading-9">
                Frequently-asked questions
            </h2>
            <dl className="w-full bg-gray-50 px-10 py-10 rounded-2xl md:w-2/3">
                <dt className="mb-4">
                    <h3 className="text-xl font-semibold">
                        What is the <span className='text-orange-600'>pricing</span>? 
                    </h3>
                </dt>
                <dd className="mb-16">
                    <p>
                        For each project we set different costing becuase it totally depends on the project type, size, features, requirements. It could be low or high as per the requirements but we are always their for partial payments and bargaining.
                    </p>
                </dd>
                <dt className="mb-4">
                    <h3 className="text-xl font-semibold">
                        How long will it take <span className='text-indigo-600'>project</span> to be LIVE?
                    </h3>
                </dt>
                <dd className="mb-16">
                    <p>
                        Project with static pages and less requirements usually takes 2 weeks or less whereas The projects with more functions and more with Designing & Development require more than 3 Week but we try to keep it as short as we can. <span className='text-indigo-600'>We always respect your Timeline : D</span>
                    </p>
                </dd>
                <dt className="mb-4">
                    <h3 className="text-xl font-semibold">
                        How to <span className='text-indigo-600'>start</span> with us?
                    </h3>
                </dt>
                <dd className="mb-16">
                    <p>
                    Our executive will call you to discuss your requirements, and you will receive a quote once we understand your need, we can bargain on pricing later. We begin working on the project with your thoughts and comments when you accept the offer. We sign contract with you that ensures we are orking on track and based on your requirement.
                    </p>
                </dd>
            </dl>
        </div>
    </div>
    </>

  )
}

export default Faq