import React from 'react'

const AboutHero = () => {
  return (
    <>
    <div className="relative bg-white overflow-hidden container">
    <div className="max-w-7xl mx-auto">
    <div className="relative z-10 pt-5 pb-8 bg-white sm:pb-16 md:pb-20 lg:max-w-2xl lg:w-full lg:pb-28 xl:pb-32">
    
    <main className=" mx-auto max-w-7xl px-4 sm:px-6 lg:px-8 ">
    <div className="sm:text-center lg:text-left">
      <h1 className="text-4xl leading-4 tracking-tight title-font font-bold text-gray-900 sm:text-5xl md:text-6xl">
        <span className="block xl:inline">Take your <span className='text-indigo-600'>business</span> to the next level.</span>{' '}
      </h1>
      <p className="mt-3 leading-4 text-base text-gray-600 sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-5 md:text-xl lg:mx-0">
      Your website serves as an electronic business card. It&apos;s the first thing customers see when they look up your business online. Our team of web designers and developers will create a professional-looking website that is suited to your needs.
To make your website truly stunning and low weight, we employ technologies like NextJs, NodeJs, MongoDB, and ExpressJs.
      </p>
      {/* <div className="mt-5 sm:mt-8 sm:flex sm:justify-center lg:justify-start">
        <div className="rounded-md shadow">
          <a
            href="#"
            className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10"
          >
            Get started
          </a>
        </div>
        <div className="mt-3 sm:mt-0 sm:ml-3">
          <a
            href="#"
            className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-indigo-700 bg-indigo-100 hover:bg-indigo-200 md:py-4 md:text-lg md:px-10"
          >
            Live demo
          </a>
        </div>
      </div> */}
    </div>
  </main>
  </div>
  </div>
  <div className="lg:absolute lg:inset-y-0 lg:right-0 w-max">
        <img
          className="h-56 w-full object-cover sm:h-72 md:h-96 lg:w-full lg:h-full"
          src="/aboutus-hero.png"
          alt=""
        />
      </div>
      </div>
    </>
  )
}

export default AboutHero