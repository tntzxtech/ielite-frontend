import React from 'react'

const HeroRight = () => {
  return (
    <>
<section className="text-gray-600 body-font">
  <div className="container mx-auto flex px-5 py-10 md:flex-row flex-col items-center">
    <div className="lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
      <h1 className="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">Wix, SquareSpace, Shopify, &<span className='text-indigo-600'> More!</span></h1>
      <p className="mb-8 leading-relaxed text-xl">Want a website on other platform ? We are always ready to design your beautifull website on any website designing platform.</p>
      <p className="text-sm mt-2 text-gray-500 mb-8 w-full">Sorry, We do not work on Wordpress for now.</p>
      <div className='flex-inline justify-between '>
      <button type="button" title="Start buying" className="btn w-full ml-4 mr-4 my-4 py-3 px-6 align-middle text-center rounded-xl transition bg-orange-600 shadow-xl hover:bg-orange-700 sm:w-max">
      <div className="flex place-content-around">
          <span className="block text-white text-lg font-semibold ml-2">
              View Templates
          </span>
      </div>
      </button>
      <button type="button" title="Start buying" className="btn w-full ml-4 my-4 mr-4 py-3 px-6 align-middle text-center rounded-xl transition bg-indigo-600 shadow-xl hover:bg-indigo-700 sm:w-max">
      <div className="flex place-content-around">
          <span className="block text-white text-lg font-semibold ml-2">
              View Client&apos;s Website
          </span>
      </div>
      </button>
      </div>
    </div>
    <div className="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 drop-shadow-xl">
      <img className="object-cover object-center rounded-lg" alt="hero" src="/services-wix-ss-shopify.png"/>
    </div>
  </div>
</section>
    </>
  )
}

export default HeroRight