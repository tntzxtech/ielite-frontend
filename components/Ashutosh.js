import React from 'react'
import Image from 'next/image'

const Ashutosh = () => {
  return (
    <>
    
<div className="py-10 bg-gray-50">
    <div className="container mx-auto px-6 md:px-12">
        <div className="mb-16 text-center">
            <h2 className="mb-4 text-center text-2xl text-gray-900 font-bold md:text-4xl">Elite Networks Founder</h2>
            <p className="text-gray-600 lg:w-8/12 lg:mx-auto">	&#34; Time is more precious than money and I do not prefer to waste my Client&#39;s time and money&#34;</p>
        </div>
        <div className="grid gap-24 md:gap-12 md:grid-cols-1">
            <div className="space-y-4 text-center group">
                <div className="w-56 h-56 mx-auto rounded-[4rem] rotate-45 overflow-hidden md:w-40 md:h-40 lg:w-56 lg:h-56">
                    <img className="w-full h-full object-cover -rotate-45 scale-125 mx-auto transition duration-300 group-hover:scale-[1.3]" 
                        src="/Ashutosh_Sir.jpg" alt="Ashutosh - Founder" loading="lazy" width="640" height="805"/>
                </div>
                <div className="pt-4">
                    <h4 className="text-2xl font-serif">Ashutosh Yadav</h4>
                    <span className="block text-sm text-gray-500">Founder & Director</span>
                </div>
                <div className="flex justify-center space-x-4 text-gray-500">
                    <a href="https://www.linkedin.com/in/ashutoshyadavofficial/" rel="noreferrer" target="_blank" aria-label="linkedIn">
                        <svg xmlns="http://www.w3.org/2000/svg" className="animate-bounce w-10 hover:text-blue-600b i bi-linkedin hover:text-blue-700" fill="currentColor"  viewBox="0 0 16 16">
                            <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z"/>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
                                
    </>
  )
}

export default Ashutosh