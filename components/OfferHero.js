import React from 'react'
import { PhoneIcon } from '@heroicons/react/outline'
import Link from 'next/link'

const OfferHero = () => {
  return (
    <>
    
<div className="py-4 bg-white">
    <div  className="container m-auto px-6 space-y-8 text-gray-500 md:px-12 lg:px-20">
        <div className="justify-center text-center gap-6 md:text-left md:flex lg:items-center  lg:gap-16">
            <div className="order-last mb-6 space-y-6 md:mb-0 md:w-6/12 lg:w-6/12">
                <p className="text-4xl text-gray-700 font-bold md:text-5xl leading-normal">Get website with us & get <span className="text-indigo-600 leading-normal">Hosting for 5 Years!</span> for just <span className='text-red-600 leading-normal'>100$AUD</span></p>
                <p className="text-2xl ">We offer 5 Years of hosting plan just for 100$.</p>
                <div className="flex flex-row-reverse flex-wrap justify-center gap-4 md:gap-6 md:justify-end">
                
                    <button type="button" title="Start buying" className="btn w-full py-3 px-6 align-middle text-center rounded-xl transition bg-orange-600 shadow-xl hover:bg-orange-700 sm:w-max">
                    <div className="flex place-content-around">
                        <Link href="/services">
                        <span className="block text-white text-lg font-semibold ml-2">
                            Know more !
                        </span>
                        </Link>
                    </div>
                    </button>
                  
                </div>
            </div>
            <div className="grid grid-cols-5 grid-rows-4 gap-4 md:w-5/12 lg:w-6/12">
                <div className="col-span-2 row-span-4">
                    <img src="/gears.png" className="rounded-full" width="640" height="960" alt="shoes" loading="lazy"/>
                </div>
                <div className="col-span-3 row-span-4">
                    <img src="/cyborg-114.png" className="w-full h-full object-cover object-top rounded-xl" width="640" height="640" alt="shoe" loading="lazy"/>
                </div>
            </div>
        </div>
    </div>
</div> 
                                
    </>
  )
}

export default OfferHero